from random import randint
import datetime

long_list = [randint(0, 3000) for element in range(1000000)]

def indexSearch(search_list, search_element):
    try:
        search_list.index(search_element)
    except ValueError:
        return False
    else:
        return True

def setSearch(search_list, search_element):
    search_list_set = set(search_list)
    search_list_set_len = len(search_list_set)
    search_list_set.add(search_element)
    if len(search_list_set) == search_list_set_len:
        return True
    else:
        return False
        
startIndexSearch = datetime.datetime.now()
indexSearch(long_list, -1)
print(datetime.datetime.now() - startIndexSearch)

startSetSearch = datetime.datetime.now()
setSearch(long_list, -1)
print(datetime.datetime.now() - startSetSearch)