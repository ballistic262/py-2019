word = 'guess'
guessWord = ''

maxGuesses = 10
guessCounter = 0

while word != guessWord:
    if guessCounter == maxGuesses:
        print('You missed!')
        break
    guessWord = input('Type the word to guess: ').lower()
    guessCounter += 1
else:
    if guessCounter == 1:
        print('You hit in first attempt!')
    else:
        print('You hit with ' + str(guessCounter) + ' attempts!')