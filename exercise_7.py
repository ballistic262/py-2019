from datetime import datetime

def person_print(name, last_name, *others, age):
    formatted_data = 'Imię: {}, nazwisko: {}, wiek: {}'.format(name,last_name,age)
    others_str = ' '
    for arg in others:
        others_str += arg + ' '
    print(formatted_data + others_str)

person_print('Walter', 'White', 'Heisenberg', 'Huntington', 'Pontiac Aztek', age = datetime.now().year - 1958)

def print_args_kwargs(first_arg, *args, middle_arg, **kwargs):
    print('first_arg: {}\nlast_arg: {}'.format(first_arg, middle_arg))
    args_str = 'args: '
    for arg in args:
        args_str += arg + ' '
    print(args_str)
    for key, value in kwargs.items():
        print("{}: {}".format(key, value))


print_args_kwargs('one', 'three', 'four', 'five', 'six', middle_arg = "two", kwargs_1 = 'seven', kwargs_2 = 'eight', kwargs_3 = 'nine')