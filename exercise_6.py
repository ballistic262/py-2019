from copy import deepcopy

di = {'one': [1], 'two': [2], 'three': [3], 'four': [4]}
copy_di = deepcopy(di)
di['four'][0] = 'cztery'

print(di)
print(copy_di)