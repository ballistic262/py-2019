import smtplib
import ssl

smtp_server = "smtp.wp.pl"
port = 465
sender = "pytest@wp.pl"

password = input("Podaj haslo do skrzynki pocztowej:")
receiver = input("Podaj mail odbiorcy:")
subject = input("Podaj tytul maila:")
message = input("Podaj tresc maila:")

mail = """\
From: {}
To: {}
Subject: {}\n
{}
""".format(sender, receiver, subject, message)

context = ssl.create_default_context()

try:
    server = smtplib.SMTP_SSL(smtp_server, port, context=context)
    server.login(sender, password)
    server.sendmail(sender, receiver, mail)

except smtplib.SMTPAuthenticationError:
    print("Blad autoryzacji z serwerem. Sprobuj ponownie!")
except smtplib.SMTPDataError:
    print("Odrzucono dane wiadomosci. Sprobuj ponownie!")
except smtplib.SMTPConnectError:
    print("Blad podczas ustawiania polaczenia z serwerem. Sprobuj ponownie!")
except smtplib.SMTPHeloError:
    print("Serwer odrzucil 'powitanie' HELO. Sprobuj ponownie!")
except smtplib.SMTPSenderRefused:
    print("Odrzucono adres nadawcy. Sprobuj ponownie!")
except smtplib.SMTPResponseException:
    print("Serwer SMTP zwrocil bledna odpowiedz. Sprobuj ponownie!")
except smtplib.SMTPRecipientsRefused:
    print("Odrzucono adres odbiorcy. Sprobuj ponownie!")
except smtplib.SMTPNotSupportedError:
    print("Serwer nie obsluguje wybranej komendy. Sprobuj ponownie!")
except smtplib.SMTPServerDisconnected:
    print("Polaczenie z serwerem zostalo niespodziewanie rozlaczone.")
except smtplib.SMTPException:
    print("Wystapil nieznany blad. Sprobuj ponownie!")

finally:
    print("Wyslano!")

server.quit()
